# echo "DHCP Network"
# dhcpcd enp0s18u1u2
# systemctl start sshd
# systemctl status sshd
echo "Start installing the system after 3 seconds...([ctrl + c] cancel)"
sleep 1
echo "Start installing the system after 2 seconds...([ctrl + c] cancel)"
sleep 1 
echo "Start installing the system after 1 seconds...([ctrl + c] cancel)"
sleep 1

python3 /root/list_device.py
device_name=$(</root/install_device_name)
if [ -z "$device_name" ]
then
	echo ""
else
    python3 /root/remove_lvm.py $device_name
    bash /root/restore.sh $device_name
    #python3 /root/copy_file_to_msata.py
    bash /root/shutdown.sh
fi